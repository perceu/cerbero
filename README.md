# Cerbero

centralizador de logs e metricas

## Getting started

Clone o repositorio e rode as imagens para captura de logs & métricas.

```
git clone https://gitlab.com/perceu/cerbero.git
cd cerbero
docker-compose up -d
```

## Integrate with your tools

- See examples in [Examples](https://gitlab.com/perceu/cerbero/-/tree/main/examples)