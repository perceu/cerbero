# Cerbero Samples

Como enviar metricas para o prometheus de um app python e como enviar logs para o influxdb. 
Tudo isso para criar paineis no Grafana.

## Getting started

Clone o repositorio e rode os exemplos para captura de logs & métricas.

```
git clone https://gitlab.com/perceu/cerbero.git
cd cerbero/examples/
make
```

Lembre de Criar o arquivo `.env` para colocar as rotas e as chaves de autenticação.

```
INFLUX_TOKEN="Leia como pegar o token na documentação"
INFLUX_ORG="xx"
INFLUX_BUCKET="xx"
INFLUX_URL="http://localhost:8086"
PROMETHEUS_PUSHGATEWAY="localhost:9091"
```

Depois de configurado:

```
source .venv/bin/activate
python send_to_influx.py
python send_to_prometheus.py
```
