from random import randint
from time import sleep
from prometheus_client import CollectorRegistry, Counter, push_to_gateway
from decouple import config
from loguru import logger

for i in range(randint(5,20)):
    sleep(10)
    registry = CollectorRegistry()
    c = Counter('requests_received', 'Numero de requests recebidas', registry=registry)
    total_requests = randint(5,20)
    logger.info(f"Total de requests {total_requests}")
    c.inc(total_requests)
    push_to_gateway(config('PROMETHEUS_PUSHGATEWAY'), job='example', registry=registry)