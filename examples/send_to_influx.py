import os
import logging
from loguru import logger
from datetime import datetime
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from decouple import config


# You can generate an API token from the "API Tokens Tab" in the UI

class PropagateHandler(logging.Handler):
    def emit(self, record):
        
        token = config('INFLUX_TOKEN')
        org = config('INFLUX_ORG')
        bucket = config('INFLUX_BUCKET')
        url = config('INFLUX_URL')

        with InfluxDBClient(url=url, token=token, org=org) as client:
            write_api = client.write_api(write_options=SYNCHRONOUS)
            point = Point(record.levelname)\
                .tag("project", "tests")\
                .field("log_message", record.getMessage())\
                .time(datetime.utcnow(), WritePrecision.NS)
            write_api.write(bucket, org, point)

logger.add(PropagateHandler())

if not os.path.exists('log.log'):
    with open('log.log', 'w') as file:
        file.write(f'0')

with open('log.log', 'r') as file:
    lines = file.readlines()
    last_line = int(lines[-1])
    logger.debug(f'message : {last_line}')
    logger.info(f'message : {last_line}')
    logger.warning(f'message : {last_line}')
    logger.error(f'message : {last_line}')
    logger.success(f'message : {last_line}')

with open('log.log', 'a+') as file:
    sum_line = last_line+1
    file.write(f'\n{sum_line}')